const getTodos = () => fetch('https://jsonplaceholder.typicode.com/todos');
const getUsersDetails = (id) => fetch('https://jsonplaceholder.typicode.com/users?id='+id);

getTodos().then(response => response.json())
.then(todosData => getUsersDetails(todosData[0].id))
.then(response => response.json())
.then(firstUserData => console.log(firstUserData))
.catch(error => console.error(error.message));
