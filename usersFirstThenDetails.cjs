const getUsers = () => fetch('https://jsonplaceholder.typicode.com/users');
const getUsersDetails = (id) => fetch('https://jsonplaceholder.typicode.com/users?id=' + id);


getUsers().then(response => response.json()
).then((usersData) => {
    let usersDetailsArray = usersData.map(({ id }) => getUsersDetails(id))
    return Promise.all(usersDetailsArray)
}).then((usersDetailsUnconverted) => {
    let usersDetailsConverted = usersDetailsUnconverted.map((item) => item.json())
    return Promise.all(usersDetailsConverted);
}).then((usersDetails) => {
    console.log(usersDetails);
}).catch((error) => {
    console.error(error);
})