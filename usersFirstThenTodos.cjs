fetch('https://jsonplaceholder.typicode.com/users').then(response => response.json())
    .then(usersData => {
        return Promise.all([usersData, fetch('https://jsonplaceholder.typicode.com/todos')]);
    }).then(([usersData, todosResponse]) => {
        return Promise.all([usersData, todosResponse.json()]);
    }).then(([usersData,todosData]) => {
        console.log(usersData);
        console.log(todosData);
    }).catch(error => console.error(error.message));
